import pytest
from django.core import mail
pytestmark = pytest.mark.django_db
from django.test import RequestFactory
from django.contrib.auth.models import AnonymousUser
from mixer.backend.django import mixer
from mock import patch
from .. import views
from django.http import Http404


class TestHomeView:
    def test_anonymous(self):
        req = RequestFactory().get('/')
        resp = views.HomeView.as_view()(req)
        assert resp.status_code == 200, 'Should be  callable by anyone'


class TestAdminView:
    def test_anonymous(self):
        req = RequestFactory().get('/')
        req.user = AnonymousUser()
        resp = views.AdminView.as_view()(req)
        assert 'login' in resp.url

    def test_superuser(self):
        user = mixer.blend('auth.User', is_superuser=True)
        req = RequestFactory().get('/')
        req.user = user
        resp = views.AdminView.as_view()(req)
        assert resp.status_code == 200, 'Authenticated user can access'

class TestPostUpdateView:
    def test_get(self):
        req = RequestFactory().get('/')
        obj = mixer.blend('birdie.Post')
        resp = views.PostUpdateView.as_view()(req)
        assert resp.status_code == 200, 'Should be callable'

    def test_post(self):
        post = mixer.blend('birdie.Post')
        data = {'body': "new body text!"}
        req = RequestFactory().post('/',data=data)
        req.user = AnonymousUser()
        resp = views.PostUpdateView.as_view()(req, pk=post.pk)
        assert resp.status_code == 302, 'Should redirect success view'
        post.refresh_from_db()
        assert post.body == 'New Body Text' , 'Should update post'

    def test_security(self):
        user = mixer.blend('auth.User',first_name='Martin')
        post = mixer.blend('birdie.post')
        req = RequestFactory().post('/', date={})
        req.user = user
        with pytest.raises(Http404):
            views.PostUpateView.as_view()(req, pk=post.pk)


class TestPaymentView:
    @patch('birdie.views.stripe')
    def test_payment(self, mock_stripe):
        mock_stripe.Charge.return_value = {'id':'234'}
        req = RequestFactory().post('/', data={'token':'123'})
        resp = views.PaymentView.as_view()(req)
        assert resp.status_code == 302, "Should re-direct to success url"
        assert len(mail.outbox) == 1, 'Should send an email'