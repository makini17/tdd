import pytest
from django import forms
from . import models
pytestmark = pytest.mark.django_db


class PostForm(forms.ModelForm):
    class Meta:
        model = models.Post
        fields = ('body',)

    def clean_body(self):
        data = self.cleaned_data.get('body')
        if len(data) <= 5:
            raise forms.ValidationError('message is too short')
        return data
